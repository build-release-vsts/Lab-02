# CI/CD with Azure DevOps
Lab 02: Creating your first CI Pipeline with Azure DevOps

---

# Tasks

 - Import a project repository
 
 - Create a CI pipeline

---

## Import a project repository

 - Browse to Azure Repos:

<img alt="Image 1.1" src="images/task-1.1.png"  width="75%" height="75%">

 - Select "Import repository" from the GIT section in the toolbar:

<img alt="Image 1.2" src="images/task-1.2.png"  width="75%" height="75%">


 - Import the repository below:
 
```
https://oauth2:u9XWV6qNpU7z9PzWRHF5@gitlab.com/build-release-vsts/dotnetcore-demo-app.git
```

<img alt="Image 1.3" src="images/task-1.3.png"  width="75%" height="75%">

---
 
## Create a Build Definition

 - Browse to Azure Pipelines (Builds):

<img alt="Image 2.1" src="images/task-2.1.png"  width="75%" height="75%">


 - Create a new pipeline

<img alt="Image 2.2" src="images/task-2.2.png"  width="75%" height="75%">

  
 - Create the build pipeline using the visual editor:

<img alt="Image build-01" src="images/build-01.PNG"  width="75%" height="75%">


 - Select the imported repository as build sources:

<img alt="Image build-02" src="images/build-02.PNG"  width="75%" height="75%">


 - Select the "Empty Job" template

 ![Image 3](images/lab02-3.png)


 - Configure the pipeline to run in the Sela pool:

```
Agent Pool: my-pool
```

<img alt="Image build-03" src="images/build-03.PNG"  width="75%" height="75%">


 - Add/Set variables for the build configuration and platform (under "variables" tab):

```
BuildConfiguration: Release
BuildPlatform: any cpu
```

<img alt="Image build-04" src="images/build-04.PNG"  width="75%" height="75%">
  
 - Go to tasks and uner **Agent Job** click on the "+" sign
 - Add a "**.NET Core**" step:

<img alt="Image build-04" src="images/add_task.PNG"  width="75%" height="75%">

 - Fill the data according to the attributes below
```
Display Name: Restore
Command: restore
Path to project(s): **/*.csproj
```

 ![Image 4](images/lab02-4.png)
 
 - Add a "**.NET Core**" step:

```
Display Name: Build
Command: build
Path to project(s): **/*.csproj
Arguments: --configuration $(BuildConfiguration)
```

 ![Image 5](images/lab02-5.png)
 
 - Add a "**.NET Core**" step:

```
Display Name: Test
Command: test
Path to project(s): **/*[Tt]ests/*.csproj
Arguments: --configuration $(BuildConfiguration)
```

 ![Image 6](images/lab02-6.png)

 - Add a "**.NET Core**" step:

```
Display Name: Publish
Command: publish
Arguments: --configuration $(BuildConfiguration) --output $(build.artifactstagingdirectory)
```

 ![Image 7](images/lab02-7.png)
 
 - Now we are going to add another type of step:
 - Add a "**Publish Build Artifacts**" step:

```
Display Name: Publish Artifact
Path to publish: $(build.artifactstagingdirectory)
Artifact name: drop
Artifact publish location: Azure Pipelines
```

 ![Image 8](images/lab02-8.png)

 
 - Update the BuildNumber format (under "Options" tab):

```
Build number format: $(BuildDefinitionName)-$(Rev:.r)
```

<img alt="Image 2.9" src="images/task-2.9.png"  width="75%" height="75%">
 
 
 - Configure the build to run nightly at 22:00 (under "Trigger" tab):


<img alt="Image 2.10" src="images/task-2.10.png"  width="75%" height="75%">
 
 
 - Save and queue the build
 
 - Watch the running build.

<img alt="Image 2.10" src="images/final_screen.png"  width="75%" height="75%">
 

